If you like what I do consider to buy me a coffee ! 
https://www.buymeacoffee.com/rondelligiacomo


# Project Report #

Scheduler for personal tasks for Android devices - University project, June 2019.

### Purpose of the application ###

The scheduler for personal tasks is an interactive application able to manage personal tasks within
a calendar, send notifications, modify tasks and mark them as completed or postpone them.

### Expected functionality ###

* Create and modify tasks : The application has to notify the user when a task is due;
* Notifications : The application must be able to create, modify and delete tasks;
* Usage graphs : The application should be able to collect usage stats.

### Planning and implementation choices ###

viewmodel as a pattern in order to facilitate the development.
The main packages are three:
-drawable;
-layout;
-com.example.task_manager.
The drawable package contain the resources of the icons and their colors.
The layout package contain the xml files of the portrait and landscape views of the app, they store
how the application will be displayed to the users.
The com.example.task_manager contain many packages, they are:
-base à contain BaseActivity class, the father of all the activities;
-dependency injection à contain application component and application module classes.
They define the method for the dependency injection and how the dependency injection is
performed;
-model à represents real state content in an object-oriented approach;
-persistence à contain DAO (operations on the tables) and DTO (persistence model -> classes that
represent the data model on the tables) classes;
-repository à abstraction of data location (classes for the tasks and category that I can add);
-view à classes of what I really see when I’m using the application, they don’t survives at changes
(eg. from portrait to landscape);
-view model à abstraction of the life circle of the android interface, survives at changes (eg. from
portrait to landscape.

### Difficulties and solutions: ###

One of the difficulty that I encountered was that of parsing the date and the time of the tasks in a
Date object. I resolved it by placing a date and time picker provided by android.

### Ideas for extending the app: ###
An idea to extend the app can be the one of make also a web app and store the tasks on a web
database in order to see them on more devices than the smartphone where the app is installed.

### Ideas for extending the app: ###
It was very interesting to program an app for android.
I liked this project. This project seemed to me to be the one with the greatest potential among
those I’ve done in the last few years. I want to learn more about the app development, but instead
of using only android I want to see the potential of new framework like ionic or flutter to make
apps.