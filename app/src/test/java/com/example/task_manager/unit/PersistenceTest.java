package com.example.task_manager.unit;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.task_manager.BlockingLiveData;
import com.example.task_manager.model.Category;
import com.example.task_manager.persistence.ApplicationDatabase;
import com.example.task_manager.persistence.dao.Categories;
import com.example.task_manager.repository.CategoryRepository;
import com.example.task_manager.repository.TaskRepository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import java.util.List;

@RunWith(AndroidJUnit4.class) @Config(manifest = Config.NONE)
public class PersistenceTest {
    @Rule public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    @Rule public ExpectedException exception = ExpectedException.none();

    private ApplicationDatabase applicationDatabase;

    private CategoryRepository categoryRepository;
    private TaskRepository taskRepository;

    public PersistenceTest() {

    }

    private ApplicationDatabase applicationDatabase() {
        return (applicationDatabase);
    }

    private CategoryRepository categoryRepository() {
        return (categoryRepository);
    }

    private TaskRepository taskRepository() {
        return (taskRepository);
    }

    @Before
    public void initialize() {
        Context context = InstrumentationRegistry.getInstrumentation().getContext();

        applicationDatabase = Room
            .inMemoryDatabaseBuilder(context, ApplicationDatabase.class)
            .allowMainThreadQueries()
            .build();

        categoryRepository = new CategoryRepository( applicationDatabase.categories() );
        taskRepository = new TaskRepository( applicationDatabase.tasks() );
    }

    @Test
    public void successful() throws InterruptedException {
        com.example.task_manager.persistence.dto.Category home = ( new com.example.task_manager.persistence.dto.Category.Builder() )
            .name("Home")
            .build();

        categoryRepository().create(home);

        com.example.task_manager.model.Category category = obtain( categoryRepository().byName("Home") );
        List<com.example.task_manager.model.Category> categories = obtain( categoryRepository().all() );

        Assert.assertNotNull(category);
        Assert.assertEquals(categories.size(), 1);
    }

    private <T> T obtain(LiveData<T> data) throws InterruptedException {
        return ( BlockingLiveData.value(data) );
    }

    @After
    public void finalize() {
        applicationDatabase().close();
    }

}