package com.example.task_manager.persistence.dto;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;

public class Date {

    @ColumnInfo(name = "year")
    @NonNull
    public Integer year;

    @ColumnInfo(name = "month")
    @NonNull
    public Integer month;

    @ColumnInfo(name = "day")
    @NonNull
    public Integer day;

    @ColumnInfo(name = "hour")
    @NonNull
    public Integer hour;

    @ColumnInfo(name = "minute")
    @NonNull
    public Integer minute;

}
