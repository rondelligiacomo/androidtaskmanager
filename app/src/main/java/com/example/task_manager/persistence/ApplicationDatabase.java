package com.example.task_manager.persistence;

import com.example.task_manager.model.Priority;
import com.example.task_manager.persistence.converter.PriorityConverter;
import com.example.task_manager.persistence.converter.StatusConverter;
import com.example.task_manager.persistence.dao.Categories;
import com.example.task_manager.persistence.dao.Tasks;
import com.example.task_manager.persistence.dto.Category;
import com.example.task_manager.persistence.dto.Task;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database( entities = { Category.class, Task.class }, version = 1, exportSchema = false )
@TypeConverters( { PriorityConverter.class, StatusConverter.class } )
public abstract class ApplicationDatabase extends RoomDatabase {

    public abstract Categories categories();
    public abstract Tasks tasks();

}
