package com.example.task_manager.persistence.converter;

import androidx.room.TypeConverter;

import com.example.task_manager.model.Status;

public class StatusConverter {

    public StatusConverter() {

    }

    @TypeConverter
    public static Status toStatus(Integer value) {
        for ( Status status : Status.values() )
            if ( status.ordinal() == value )
                return (status);

        return (null);
    }

    @TypeConverter
    public static Integer toInteger(Status value) {
        return ( value == null ? null : value.ordinal() );
    }

}
