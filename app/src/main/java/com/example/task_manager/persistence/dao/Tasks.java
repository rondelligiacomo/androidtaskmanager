package com.example.task_manager.persistence.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.task_manager.model.StatusCount;

import java.util.List;

@Dao
public interface Tasks {

    @Insert
    public void create(com.example.task_manager.persistence.dto.Task task);

    @Query(
        "SELECT " +
        "task.identifier as identifier, task.name as name, task.priority as priority, category.identifier as categoryIdentifier, category.name as categoryName, " +
        "task.year as year, task.month as month, task.day as day, task.hour as hour, task.minute as minute, task.status as status, task.description as description " +
        "FROM task, category WHERE task.category = category.identifier"
    )
    public LiveData< List<com.example.task_manager.model.Task> > all();

    @Query(
        "SELECT " +
        "task.identifier as identifier, task.name as name, task.priority as priority, category.identifier as categoryIdentifier, category.name as categoryName, " +
        "task.year as year, task.month as month, task.day as day, task.hour as hour, task.minute as minute, task.status as status, task.description as description " +
        "FROM task, category WHERE task.category = category.identifier AND task.identifier = :identifier"
    )
    public LiveData<com.example.task_manager.model.Task> byIdentifier(long identifier);

    @Query(
        "SELECT " +
        "task.identifier as identifier, task.name as name, task.priority as priority, category.identifier as categoryIdentifier, category.name as categoryName, " +
        "task.year as year, task.month as month, task.day as day, task.hour as hour, task.minute as minute, task.status as status, task.description as description " +
        "FROM task, category WHERE task.category = category.identifier AND task.year = :year AND task.month = :month AND task.day = :day"
    )
    public LiveData< List<com.example.task_manager.model.Task> > byDate(int year, int month, int day);

    @Query(
        "SELECT " +
        "task.identifier as identifier, task.name as name, task.priority as priority, category.identifier as categoryIdentifier, category.name as categoryName, " +
        "task.year as year, task.month as month, task.day as day, task.hour as hour, task.minute as minute, task.status as status, task.description as description " +
        "FROM task, category WHERE task.category = category.identifier AND task.priority = :priority"
    )
    public LiveData< List<com.example.task_manager.model.Task> > byPriority(int priority);

    @Query(
        "SELECT " +
        "task.identifier as identifier, task.name as name, task.priority as priority, category.identifier as categoryIdentifier, category.name as categoryName, " +
        "task.year as year, task.month as month, task.day as day, task.hour as hour, task.minute as minute, task.status as status, task.description as description " +
        "FROM task, category WHERE task.category = category.identifier AND task.category = :category"
    )
    public LiveData< List<com.example.task_manager.model.Task> > byCategory(int category);

    @Query("SELECT task.status as status, COUNT(*) as count FROM task GROUP BY task.status")
    public LiveData< List<StatusCount> > byStatus();

    @Update
    public void update(com.example.task_manager.persistence.dto.Task task);

    @Delete
    public void delete(com.example.task_manager.persistence.dto.Task task);

}
