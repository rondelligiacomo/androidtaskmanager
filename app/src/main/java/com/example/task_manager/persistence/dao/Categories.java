package com.example.task_manager.persistence.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Categories {

    @Insert
    public void create(com.example.task_manager.persistence.dto.Category category);

    @Query("SELECT identifier, name from category")
    public LiveData< List<com.example.task_manager.model.Category> > all();

    @Query ("SELECT * FROM category WHERE identifier = :identifier")
    public LiveData<com.example.task_manager.model.Category> byIdentifier(long identifier);

    @Query("SELECT * FROM category WHERE name = :name ")
    public LiveData<com.example.task_manager.model.Category> byName(String name);

    @Update
    public void update(com.example.task_manager.persistence.dto.Category category);

    @Delete
    public void delete(com.example.task_manager.persistence.dto.Category category);

}
