package com.example.task_manager.persistence.dto;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Index;

@Entity(
    tableName = "category",
    indices = { @Index(value = "name", unique = true) }
)
public class Category {

    @ColumnInfo(name = "identifier")
    @PrimaryKey(autoGenerate = false)
    public Long identifier;

    @ColumnInfo(name = "name")
    @NonNull
    public String name;

    public static class Builder {
        private Long identifier;
        private String name;

        public Builder() {

        }

        public Builder identifier(Long identifier) {
            this.identifier = identifier;
            return (this);
        }

        public Builder name(String name){
            this.name = name;
            return (this);
        }

        public Category build(){
            Category category = new Category();
            category.name = name;
            return (category);
        }
    }

}


