package com.example.task_manager.persistence.converter;

import androidx.room.TypeConverter;

import com.example.task_manager.model.Priority;

public class PriorityConverter {

    public PriorityConverter() {

    }

    @TypeConverter
    public static Priority toPriority(Integer value) {
        for ( Priority priority : Priority.values() )
            if ( priority.ordinal() == value )
                return (priority);

        return (null);
    }

    @TypeConverter
    public static Integer toInteger(Priority value) {
        return ( value == null ? null : value.ordinal() );
    }

}
