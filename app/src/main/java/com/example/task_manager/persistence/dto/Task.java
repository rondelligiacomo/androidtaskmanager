package com.example.task_manager.persistence.dto;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.task_manager.model.Priority;
import com.example.task_manager.model.Status;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
    tableName = "task",
    foreignKeys = {
        @ForeignKey( entity = Category.class, parentColumns = "identifier", childColumns = "category", onDelete = CASCADE, onUpdate = CASCADE )
    },
    indices = { @Index(value = "category") }
)
public class Task {

    @ColumnInfo(name = "identifier")
    @PrimaryKey(autoGenerate = false)
    public Long identifier;

    @ColumnInfo(name = "name")
    @NonNull
    public String name;

    @ColumnInfo(name = "priority")
    @NonNull
    public Priority priority;

    @ColumnInfo(name = "category")
    @NonNull
    public Long category;

    @NonNull @Embedded
    public Date date;

    @ColumnInfo(name = "status")
    @NonNull
    public Status status;

    @ColumnInfo(name = "description")
    public String description;

    public static class Builder {
        private Long identifier;
        private String name;
        private Priority priority;
        private Long category;
        private Date date;
        private Status status;
        private String description;

        public Builder() {

        }

        public Builder identifier(Long identifier) {
            this.identifier = identifier;
            return (this);
        }

        public Builder name(String name){
            this.name = name;
            return (this);
        }

        public Builder priority(Priority priority) {
            this.priority = priority;
            return (this);
        }

        public Builder category(Long category) {
            this.category = category;
            return (this);
        }

        public Builder date(Integer year, Integer month, Integer day, Integer hour, Integer minute) {
            Date date = new Date();
            date.year = year;
            date.month = month;
            date.day = day;
            date.hour = hour;
            date.minute = minute;
            this.date = date;
            return (this);
        }

        public Builder status(Status status) {
            this.status = status;
            return (this);
        }

        public Builder description(String description) {
            this.description = description;
            return (this);
        }

        public Task build(){
            Task task = new Task();
            task.identifier = identifier;
            task.name = name;
            task.priority = priority;
            task.category = category;
            task.date = date;
            task.status = status;
            task.description = description;
            return (task);
        }
    }

}

