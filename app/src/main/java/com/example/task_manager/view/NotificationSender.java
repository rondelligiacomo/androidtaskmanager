package com.example.task_manager.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.HashMap;

public class NotificationSender {

    private NotificationSender() {

    }

    public static void send(Context context, long identifier, int year, int month, int day, int hour, int minute) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent notificationReceiver = new Intent(context, NotificationReceiver.class);
        notificationReceiver.putExtra( "identifier", identifier );

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationReceiver, PendingIntent.FLAG_UPDATE_CURRENT);

        LocalDateTime date = LocalDateTime.of(year, month, day, hour, minute);
        long milliseconds = date.toInstant( OffsetDateTime.now().getOffset() ).toEpochMilli();

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, milliseconds, pendingIntent);

        Notification.add(identifier, pendingIntent);
    }

}
