package com.example.task_manager.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.example.task_manager.R;

import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimePicker extends DialogFragment {
    private DateFormat dateFormatter, timeFormatter;

    private SublimePicker picker;

    private Callback callback;

    SublimeListenerAdapter listener = new SublimeListenerAdapter() {

        @Override
        public void onCancelled() {
            dismiss();
        }

        @Override
        public void onDateTimeRecurrenceSet(
            SublimePicker picker,
            SelectedDate date,
            int hour,
            int minute,
            SublimeRecurrencePicker.RecurrenceOption option,
            String recurrence
        ) {
            if (callback != null) {
                Calendar calendar = date.getFirstDate();
                long epoch = calendar.getTimeInMillis();
                LocalDateTime selected = LocalDateTime.ofInstant( Instant.ofEpochMilli(epoch), calendar.getTimeZone().toZoneId() );

                int year = selected.getYear();
                int month = selected.getMonth().getValue();
                int day = selected.getDayOfMonth();

                callback.selected(year, month, day, hour, minute);
            }

            dismiss();
        }

    };

    public DateTimePicker() {
        this(null);
    }

    public DateTimePicker(Callback callback) {
        dateFormatter = DateFormat.getDateInstance( DateFormat.MEDIUM, Locale.getDefault() );
        timeFormatter = DateFormat.getTimeInstance( DateFormat.SHORT, Locale.getDefault() );
        timeFormatter.setTimeZone( TimeZone.getDefault() );

        setCallback(callback);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        picker = (SublimePicker) getActivity().getLayoutInflater().inflate(R.layout.sublime_picker, container);

        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER | SublimeOptions.ACTIVATE_TIME_PICKER);

        picker.initializePicker(options, listener);
        return (picker);
    }

    public interface Callback {

        public void selected(int year, int month, int day, int hour, int minute);

    }

}
