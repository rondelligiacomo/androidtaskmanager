package com.example.task_manager.view;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LifecycleService;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.example.task_manager.R;
import com.example.task_manager.TaskManager;
import com.example.task_manager.model.Status;
import com.example.task_manager.model.Task;
import com.example.task_manager.repository.TaskRepository;

public class NotificationService extends LifecycleService {
    private static final int IMPORTANCE = NotificationManager.IMPORTANCE_DEFAULT;
    private static final String CHANNEL_NAME = "channel";

    private LiveData<Task> taskLiveData;
    private Observer<Task> observer = (task) -> notification(task);

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long identifier = intent.getLongExtra("identifier", 0);

        TaskManager taskManager = (TaskManager) this.getApplicationContext();
        TaskRepository taskRepository = taskManager.applicationComponent().taskRepository();
        taskLiveData = taskRepository.byIdentifier(identifier);
        taskLiveData.observe(this, observer);

        return ( super.onStartCommand(intent, flags, startId) );
    }

    private void notification(Task task) {
        taskLiveData.removeObserver(observer);

        Context context = getApplicationContext();

        long identifier = task.identifier();

        if ( task.status() == Status.PENDING ) {

            Bundle bundle = new Bundle();

            bundle.putLong("identifier", identifier);
            bundle.putString("name", task.name());
            bundle.putString("priority", String.valueOf( task.priority()) );
            bundle.putLong("categoryIdentifier", task.categoryIdentifier());
            bundle.putString("categoryName", task.categoryName());
            bundle.putInt("year", task.year());
            bundle.putInt("month", task.month());
            bundle.putInt("day", task.day());
            bundle.putInt("hour", task.hour());
            bundle.putInt("minute", task.minute());
            bundle.putString("description", task.description());

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_NAME, CHANNEL_NAME, IMPORTANCE);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);

            Intent mainActivity = new Intent(context, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 1, mainActivity, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent ongoingService = new Intent(context, OngoingService.class);
            ongoingService.putExtra("bundle", bundle);
            PendingIntent ongoingIntent = PendingIntent.getService(context, (int) task.identifier(), ongoingService, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent postponeService = new Intent(context, PostponeService.class);
            postponeService.putExtra("bundle", bundle);
            PendingIntent postponeIntent = PendingIntent.getService(context, (int) task.identifier(), postponeService, 0);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_NAME)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(task.name())
                .setContentText(task.categoryName())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(contentIntent)
                .addAction(R.drawable.ic_launcher_background, "Ongoing", ongoingIntent)
                .addAction(R.drawable.ic_launcher_background, "Postpone", postponeIntent)
                .setAutoCancel(true);

            notificationManager.notify( (int) identifier, notificationBuilder.build() );

            Notification.cancel(identifier);
        } else {
            Toast.makeText(context, "Invalid task status", Toast.LENGTH_SHORT).show();
        }
    }

}
