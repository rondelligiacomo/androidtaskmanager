package com.example.task_manager.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleService;

import com.example.task_manager.TaskManager;
import com.example.task_manager.model.Priority;
import com.example.task_manager.model.Status;
import com.example.task_manager.repository.TaskRepository;

import java.time.LocalDateTime;

public class PostponeService extends LifecycleService {
    private TaskRepository taskRepository;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getBundleExtra("bundle");

        String name = bundle.getString("name");
        long identifier = bundle.getLong("identifier");

        Priority priority = Priority.valueOf( bundle.getString("priority") );
        long categoryIdentifier = bundle.getLong("categoryIdentifier", 0);
        int year = bundle.getInt("year", 0);
        int month = bundle.getInt("month", 0);
        int day = bundle.getInt("day", 0);
        int hour = bundle.getInt("hour", 0);
        int minute = bundle.getInt("minute", 0);
        String description = bundle.getString("description");
        TaskManager taskManager = (TaskManager) this.getApplicationContext();
        taskRepository = taskManager.applicationComponent().taskRepository();

        LocalDateTime date = LocalDateTime.now();
        date = date.minusSeconds( date.getSecond() );
        date = date.plusMinutes(15);

        Toast.makeText(this, "Postponed at: " + date.toString(), Toast.LENGTH_SHORT).show();

        year = date.getYear();
        month = date.getMonth().getValue();
        day = date.getDayOfMonth();
        hour = date.getHour();
        minute = date.getMinute();

        update(identifier, name, priority, categoryIdentifier, year, month, day, hour, minute, description);

        return ( super.onStartCommand(intent, flags, startId) );
    }

    private void update(long identifier, String name, Priority priority, long categoryIdentifier, int year, int month, int day, int hour, int minute, String description) {
        com.example.task_manager.persistence.dto.Task task = ( new com.example.task_manager.persistence.dto.Task.Builder() )
            .identifier( identifier )
            .name( name )
            .priority( priority )
            .category( categoryIdentifier)
            .date( year, month, day, hour, minute )
            .status( Status.PENDING )
            .description( description )
            .build();

        taskRepository.update(task);

        NotificationSender.send(this, identifier, year, month, day, hour, minute);
    }
}
