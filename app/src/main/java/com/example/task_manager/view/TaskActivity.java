package com.example.task_manager.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.task_manager.R;
import com.example.task_manager.TaskManager;
import com.example.task_manager.model.Category;
import com.example.task_manager.model.Priority;
import com.example.task_manager.model.Status;
import com.example.task_manager.persistence.dto.Task;
import com.example.task_manager.view_model.CategoryViewModel;
import com.example.task_manager.view_model.TaskViewModel;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.spinnerPriority) Spinner spinnerPriority;
    @BindView(R.id.spinnerCategory) Spinner spinnerCategory;
    @BindView(R.id.textDate) TextView textDate;
    @BindView(R.id.editDescription) EditText editDescription;

    private int year, month, day;
    private int hour, minute;

    @Inject ViewModelProvider.Factory viewModelFactory;

    private CategoryViewModel categoryViewModel;
    private TaskViewModel taskViewModel;

    private com.example.task_manager.model.Task existingTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        ButterKnife.bind(this);

        TaskManager taskManager = (TaskManager) this.getApplication();
        taskManager.applicationComponent().inject(TaskActivity.this);

        toolbar();

        categoryViewModel = ViewModelProviders.of(TaskActivity.this, viewModelFactory).get(CategoryViewModel.class);
        taskViewModel = ViewModelProviders.of(TaskActivity.this, viewModelFactory).get(TaskViewModel.class);

        ArrayAdapter<Priority> items = new ArrayAdapter<>(
            TaskActivity.this,
            android.R.layout.simple_spinner_dropdown_item,
            Priority.values()
        );

        spinnerPriority.setAdapter(items);

        categoryViewModel.all().observe(TaskActivity.this, (categories) -> onCategoryChange(categories) );

        LocalDateTime now = LocalDateTime.now();
        int year = now.getYear();
        int month = now.getMonth().getValue();
        int day = now.getDayOfMonth();

        int hour = now.getHour();
        int minute = now.getMinute();

        onDateChange(year, month, day, hour, minute);
    }

    private void loadTask(com.example.task_manager.model.Task task) {
        editName.setText( task.name() );

        ArrayAdapter<Priority> adapterPriority = (ArrayAdapter<Priority>) spinnerPriority.getAdapter();
        for (int position = 0; position < adapterPriority.getCount(); position++) {
            Priority priority = adapterPriority.getItem(position);
            if (priority == task.priority()) {
                spinnerPriority.setSelection(position);
            }
        }

        ArrayAdapter<Category> adapterCategory = (ArrayAdapter<Category>) spinnerCategory.getAdapter();
        for (int position = 0; position < adapterCategory.getCount(); position++) {
            Category category = adapterCategory.getItem(position);
            if (category.identifier() == task.categoryIdentifier()) {
                spinnerCategory.setSelection(position);
            }
        }

        this.year = task.year();
        this.month = task.month();
        this.day = task.day();
        this.hour = task.hour();
        this.minute = task.minute();

        LocalDateTime date = LocalDateTime.of(year, month, day, hour, minute);
        textDate.setText( "Date/Time: " + date.toLocalDate().toString() + " " + date.toLocalTime().toString() );

        editDescription.setText(task.description());

        existingTask = task;
    }

    private void toolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void onCategoryChange(List<Category> categories) {
        ArrayAdapter<Category> items = new ArrayAdapter<>(
            TaskActivity.this,
            android.R.layout.simple_spinner_dropdown_item,
            categories
        );

        items.insert( new Category(-1, "None"), 0 );

        spinnerCategory.setAdapter(items);

        Intent intent = getIntent();
        if ( intent != null ) {
            if ( intent.hasExtra("identifier") ) {
                long identifier = intent.getLongExtra("identifier", 0);

                taskViewModel.byIdentifier(identifier).observe(TaskActivity.this, (task) -> loadTask(task) );
            }
        }
    }

    @OnClick(R.id.buttonDate)
    public void onDate() {
        DateTimePicker picker = new DateTimePicker( (year, month, day, hour, minute) -> onDateChange(year, month, day, hour, minute) );

        picker.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        picker.show( this.getSupportFragmentManager(), "Select Date and Time" );
    }

    private void onDateChange(int year, int month, int day, int hour, int minute) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;

        LocalDateTime date = LocalDateTime.of(year, month, day, hour, minute);
        textDate.setText( "Date/Time: " + date.toLocalDate().toString() + " " + date.toLocalTime().toString() );
    }

    @OnClick(R.id.buttonOk)
    public void onOk() {
        if ( existingTask == null ) {
            long identifier = LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli();

            String name = editName.getText().toString();
            Priority priority = (Priority) spinnerPriority.getSelectedItem();
            Category category = (Category) spinnerCategory.getSelectedItem();
            String description = editDescription.getText().toString();

            Task task = (new Task.Builder())
                    .identifier(identifier)
                    .name(name)
                    .priority(priority)
                    .category(category.identifier())
                    .date(year, month, day, hour, minute)
                    .status(Status.PENDING)
                    .description(description)
                    .build();

            taskViewModel.create(task);

            NotificationSender.send( getApplicationContext(), identifier, year, month, day, hour, minute );
        } else {
            String name = editName.getText().toString();
            Priority priority = (Priority) spinnerPriority.getSelectedItem();
            Category category = (Category) spinnerCategory.getSelectedItem();
            String description = editDescription.getText().toString();

            Task task = (new Task.Builder())
                .identifier(existingTask.identifier())
                .name(name)
                .priority(priority)
                .category(category.identifier())
                .date(year, month, day, hour, minute)
                .status(existingTask.status())
                .description(description)
                .build();

            taskViewModel.update(task);

            NotificationSender.send( getApplicationContext(), existingTask.identifier(), year, month, day, hour, minute );
        }

        hideKeyboard();

        finish();
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

}
