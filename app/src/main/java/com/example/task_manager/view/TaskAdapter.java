package com.example.task_manager.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.task_manager.R;
import com.example.task_manager.model.Status;
import com.example.task_manager.model.Task;
import com.example.task_manager.view_model.CategoryViewModel;
import com.example.task_manager.view_model.TaskViewModel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter< TaskAdapter.ViewHolder> {
    private Context context;

    private CategoryViewModel categoryViewModel;
    private TaskViewModel taskViewModel;

    private List<com.example.task_manager.model.Task> tasks;

    public TaskAdapter(Context context, CategoryViewModel categoryViewModel, TaskViewModel taskViewModel) {
        this.context = context;

        this.categoryViewModel = categoryViewModel;
        this.taskViewModel = taskViewModel;

        this.tasks = new ArrayList<>();
    }

    public List<com.example.task_manager.model.Task> tasks() {
        return (tasks);
    }

    public void tasks(List<com.example.task_manager.model.Task> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int resource) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.task_item, parent, false);
        return ( new ViewHolder(view) );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        com.example.task_manager.model.Task task = tasks().get(position);

        holder.textName.setText( "Task: " + task.name() );
        holder.textPriority.setText( "Priority: " + task.priority() );
        holder.textCategory.setText( "Category: " + task.categoryName() );

        LocalDateTime date = LocalDateTime.of( task.year(), task.month(), task.day(), task.hour(), task.minute() );
        holder.textDate.setText( "Date/Time: " + date.toLocalDate().toString() + " " + date.toLocalTime().toString() );

        holder.textStatus.setText( "Status: " + task.status() );

        holder.textDescription.setText( "Description: " + task.description() );

        if ( task.status() == Status.ONGOING || task.status() == Status.COMPLETED )
            holder.buttonMarkAsOngoing.setEnabled(false);
        else {
            holder.buttonMarkAsOngoing.setEnabled(true);
            holder.buttonMarkAsOngoing.setOnClickListener((view) -> markAsOngoing(task));
        }

        if ( task.status() == Status.COMPLETED )
            holder.buttonMarkAsCompleted.setEnabled(false);
        else {
            holder.buttonMarkAsCompleted.setEnabled(true);
            holder.buttonMarkAsCompleted.setOnClickListener((view) -> markAsCompleted(task));
        }

        holder.buttonUpdateTask.setOnClickListener( (view) -> onUpdateTask(task) );
        holder.buttonDeleteTask.setOnClickListener( (view) -> deleteTask(task) );
    }



    private void markAsOngoing(Task task) {
        task.ongoing();
        updateTask(task);
    }

    private void markAsCompleted(Task task) {
        task.complete();
        updateTask(task);
    }

    private void updateTask(com.example.task_manager.model.Task toUpdate) {
        com.example.task_manager.persistence.dto.Task task = ( new com.example.task_manager.persistence.dto.Task.Builder() )
            .identifier( toUpdate.identifier() )
            .name( toUpdate.name() )
            .priority( toUpdate.priority() )
            .category( toUpdate.categoryIdentifier() )
            .date( toUpdate.year(), toUpdate.month(), toUpdate.day(), toUpdate.hour(), toUpdate.minute() )
            .status( toUpdate.status() )
            .description( toUpdate.description() )
            .build();

        taskViewModel.update(task);
    }

    private void onUpdateTask(Task task) {
        Intent taskActivity = new Intent(context, TaskActivity.class);
        taskActivity.putExtra("identifier", task.identifier());
        context.startActivity(taskActivity);
    }

    private void deleteTask(com.example.task_manager.model.Task toDelete) {
        com.example.task_manager.persistence.dto.Task task = ( new com.example.task_manager.persistence.dto.Task.Builder() )
            .identifier( toDelete.identifier() )
            .name( toDelete.name() )
            .priority( toDelete.priority() )
            .category( toDelete.categoryIdentifier() )
            .date( toDelete.year(), toDelete.month(), toDelete.day(), toDelete.hour(), toDelete.minute() )
            .status( toDelete.status() )
            .description( toDelete.description() )
            .build();

        taskViewModel.delete(task);

        if ( toDelete.status() == Status.PENDING )
            Notification.cancel( toDelete.identifier() );
    }

    @Override
    public int getItemCount() {
        return ( tasks().size() );
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textName;
        private TextView textPriority;
        private TextView textCategory;
        private TextView textDate;
        private TextView textStatus;
        private TextView textDescription;
        private Button buttonMarkAsOngoing;
        private Button buttonMarkAsCompleted;
        private Button buttonUpdateTask;
        private Button buttonDeleteTask;

        public ViewHolder(View view) {
            super(view);

            textName = view.findViewById(R.id.textName);
            textPriority = view.findViewById(R.id.textPriority);
            textCategory = view.findViewById(R.id.textCategory);
            textDate = view.findViewById(R.id.textDate);
            textStatus = view.findViewById(R.id.textStatus);
            textDescription = view.findViewById(R.id.textDescription);
            buttonMarkAsOngoing = view.findViewById(R.id.buttonMarkAsOngoing);
            buttonMarkAsCompleted = view.findViewById(R.id.buttonMarkAsCompleted);
            buttonUpdateTask = view.findViewById(R.id.buttonUpdateTask);
            buttonDeleteTask = view.findViewById(R.id.buttonDeleteTask);
        }
    }

}
