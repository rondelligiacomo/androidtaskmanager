package com.example.task_manager.view;

import android.app.Activity;
import android.os.Bundle;

import com.example.task_manager.TaskManager;
import com.example.task_manager.base.BaseActivity;
import com.example.task_manager.model.Category;
import com.example.task_manager.model.Priority;
import com.example.task_manager.model.Status;
import com.example.task_manager.model.Task;
import com.example.task_manager.view_model.CategoryViewModel;
import com.example.task_manager.view_model.TaskViewModel;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.task_manager.R;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity {
    @BindView(R.id.layout) CoordinatorLayout layout;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.spinnerPriority) Spinner spinnerPriority;
    @BindView(R.id.spinnerCategory) Spinner spinnerCategory;
    @BindView(R.id.spinnerStatus) Spinner spinnerStatus;

    @BindView(R.id.recyclerTasks) RecyclerView recyclerTasks;

    @Inject ViewModelProvider.Factory viewModelFactory;

    private CategoryViewModel categoryViewModel;
    private TaskViewModel taskViewModel;

    private TaskAdapter taskAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        TaskManager taskManager = (TaskManager) this.getApplication();
        taskManager.applicationComponent().inject(SearchActivity.this);

        categoryViewModel = ViewModelProviders.of(SearchActivity.this, viewModelFactory).get(CategoryViewModel.class);
        taskViewModel = ViewModelProviders.of(SearchActivity.this, viewModelFactory).get(TaskViewModel.class);

        toolbar();

        categoryViewModel = ViewModelProviders.of(SearchActivity.this, viewModelFactory).get(CategoryViewModel.class);
        taskViewModel = ViewModelProviders.of(SearchActivity.this, viewModelFactory).get(TaskViewModel.class);

        ArrayAdapter<Priority> priorities = new ArrayAdapter<>(
            SearchActivity.this,
            android.R.layout.simple_spinner_dropdown_item,
            Priority.values()
        );

        spinnerPriority.setAdapter(priorities);

        categoryViewModel.all().observe(SearchActivity.this, (categories) -> onCategoryChange(categories) );

        recyclerTasks.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this);
        recyclerTasks.setLayoutManager(layoutManager);

        taskAdapter = new TaskAdapter(SearchActivity.this, categoryViewModel, taskViewModel);
        recyclerTasks.setAdapter(taskAdapter);

        ArrayAdapter<Status> statuses = new ArrayAdapter<>(
            SearchActivity.this,
            android.R.layout.simple_spinner_dropdown_item,
            Status.values()
        );

        spinnerStatus.setAdapter(statuses);
    }

    private void onCategoryChange(List<Category> categories) {
        ArrayAdapter<Category> items = new ArrayAdapter<>(
            SearchActivity.this,
            android.R.layout.simple_spinner_dropdown_item,
            categories
        );

        items.insert( new Category(-1, "None"), 0 );

        spinnerCategory.setAdapter(items);
    }

    private void toolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.buttonFilter)
    public void onFilter() {
        taskViewModel.all().observe(SearchActivity.this, (tasks) -> onTaskChanged(tasks) );
        hideKeyboard();
    }

    private void onTaskChanged(List<Task> tasks) {
        String name = editName.getText().toString();
        Priority priority = (Priority) spinnerPriority.getSelectedItem();
        Category category = (Category) spinnerCategory.getSelectedItem();
        Status status = (Status) spinnerStatus.getSelectedItem();

        List<Task> filtered = tasks
            .stream()
            .filter( (task) -> filterByName(task, name) )
            .filter( (task) -> filterByPriority(task, priority) )
            .filter( (task) -> filterByCategory(task, category) )
            .filter( (task) -> filterByStatus(task, status) )
            .collect( Collectors.toList() );

        taskAdapter.tasks(filtered);
    }

    private boolean filterByName(Task task, String name) {
        return ( task.name().contains(name) );
    }

    private boolean filterByPriority(Task task, Priority priority) {
        if ( priority == Priority.NO_PRIORITY )
            return (true);

        return ( task.priority() == priority );
    }

    private boolean filterByCategory(Task task, Category category) {
        if ( category.name().equals("None") )
            return (true);

        return (  task.categoryName().equals( category.name() )  );
    }

    private boolean filterByStatus(Task task, Status status) {
        if ( status == Status.NO_STATUS )
            return (true);

        return ( task.status() == status );
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

}
