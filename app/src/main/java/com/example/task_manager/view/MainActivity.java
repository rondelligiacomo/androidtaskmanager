package com.example.task_manager.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.example.task_manager.TaskManager;
import com.example.task_manager.base.BaseActivity;
import com.example.task_manager.model.DataGenerator;
import com.example.task_manager.persistence.dto.Category;
import com.example.task_manager.view_model.CategoryViewModel;
import com.example.task_manager.view_model.TaskViewModel;
import com.example.task_manager.view_model.ViewModelFactory;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.renderscript.Sampler;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;

import com.example.task_manager.R;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.calendarTasks) CalendarView calendarTasks;
    @BindView(R.id.recyclerTasks) RecyclerView recyclerTasks;

    @Inject ViewModelProvider.Factory viewModelFactory;

    private CategoryViewModel categoryViewModel;
    private TaskViewModel taskViewModel;

    private TaskAdapter taskAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        TaskManager taskManager = (TaskManager) this.getApplication();
        taskManager.applicationComponent().inject(MainActivity.this);

        toolbar();

        categoryViewModel = ViewModelProviders.of(MainActivity.this, viewModelFactory).get(CategoryViewModel.class);
        taskViewModel = ViewModelProviders.of(MainActivity.this, viewModelFactory).get(TaskViewModel.class);

        DataGenerator.generate(MainActivity.this, categoryViewModel, taskViewModel);

        calendarTasks.setOnDateChangeListener( (view, year, month, day) -> onDateChange(view, year, month + 1, day) );

        recyclerTasks.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( MainActivity.this );
        recyclerTasks.setLayoutManager(layoutManager);

        taskAdapter = new TaskAdapter(MainActivity.this, categoryViewModel, taskViewModel);
        recyclerTasks.setAdapter(taskAdapter);

        LocalDate now = LocalDate.now();
        int year = now.getYear();
        int month = now.getMonth().getValue();
        int day = now.getDayOfMonth();

        onDateChange(calendarTasks, year, month, day);
    }

    private void toolbar() {
        setSupportActionBar(toolbar);
    }

    private void onDateChange(CalendarView calendarTasks, int year, int month, int day) {
        taskViewModel.byDate(year, month, day).observe( MainActivity.this, (tasks) -> taskAdapter.tasks(tasks) );
    }

    @OnClick(R.id.buttonAddTask)
    public void onAddTask() {
        Intent taskActivity = new Intent(MainActivity.this, TaskActivity.class);
        startActivity(taskActivity);
    }

    @OnClick(R.id.buttonAddCategory)
    public void onAddCategory() {
        Intent categoryActivity = new Intent(MainActivity.this, CategoryActivity.class);
        startActivity(categoryActivity);
    }

    @OnClick(R.id.buttonSearch)
    public void onSearch() {
        Intent searchActivity = new Intent(MainActivity.this, SearchActivity.class);
        startActivity(searchActivity);
    }

    @OnClick(R.id.buttonStatistics)
    public void onStatistics() {
        Intent statisticsActivity = new Intent(MainActivity.this, StatisticsActivity.class);
        startActivity(statisticsActivity);
    }

}
