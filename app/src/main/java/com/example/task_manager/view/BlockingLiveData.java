package com.example.task_manager.view;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class BlockingLiveData {

    public static <T> T value(LiveData<T> liveData) throws InterruptedException {
        Holder<T> holder = new Holder<>();
        CountDownLatch counter = new CountDownLatch(1);

        Observer<T> observer = (T data) -> { holder.set(data); counter.countDown(); };

        liveData.observeForever(observer);
        counter.await(5, TimeUnit.SECONDS);
        liveData.removeObserver(observer);

        return ( holder.get() );
    }

}
