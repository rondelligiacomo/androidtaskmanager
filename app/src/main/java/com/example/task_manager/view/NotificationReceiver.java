package com.example.task_manager.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BroadcastReceiver {

    public NotificationReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        long identifier = intent.getLongExtra("identifier", 0);
        Intent notificationService = new Intent(context, NotificationService.class);
        notificationService.putExtra("identifier", identifier);
        context.startService(notificationService);
    }

}
