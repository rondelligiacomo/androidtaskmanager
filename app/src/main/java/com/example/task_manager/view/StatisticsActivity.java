package com.example.task_manager.view;

import android.graphics.Color;
import android.os.Bundle;

import com.example.task_manager.TaskManager;
import com.example.task_manager.base.BaseActivity;
import com.example.task_manager.model.StatusCount;
import com.example.task_manager.model.Task;
import com.example.task_manager.view_model.CategoryViewModel;
import com.example.task_manager.view_model.TaskViewModel;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.view.View;

import com.example.task_manager.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatisticsActivity extends BaseActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.chartTask) PieChart chartTask;

    @Inject ViewModelProvider.Factory viewModelFactory;

    private CategoryViewModel categoryViewModel;
    private TaskViewModel taskViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        ButterKnife.bind(this);

        TaskManager taskManager = (TaskManager) this.getApplication();
        taskManager.applicationComponent().inject(StatisticsActivity.this);

        toolbar();

        categoryViewModel = ViewModelProviders.of(StatisticsActivity.this, viewModelFactory).get(CategoryViewModel.class);
        taskViewModel = ViewModelProviders.of(StatisticsActivity.this, viewModelFactory).get(TaskViewModel.class);

        initializeChart();
    }

    private void toolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initializeChart() {
        chartTask.setUsePercentValues(true);
        chartTask.getDescription().setEnabled(false);
        chartTask.setExtraOffsets(5, 10, 5, 5);

        chartTask.setDragDecelerationFrictionCoef(0.95f);

        chartTask.setDrawCenterText(true);

        chartTask.setRotationAngle(0);
        chartTask.setRotationEnabled(false);
        chartTask.setHighlightPerTapEnabled(false);

        chartTask.animateY(1400, Easing.EaseInOutQuad);

        chartTask.getLegend().setEnabled(false);

        chartTask.setEntryLabelColor(Color.BLACK);
        chartTask.setEntryLabelTextSize(12f);

        data();
    }

    private void data() {
        taskViewModel.byStatus().observe( StatisticsActivity.this, (statusCounts) -> onDataChange(statusCounts) );
    }

    private void onDataChange(List<StatusCount> statusCounts) {
        List<PieEntry> entries = new ArrayList<>();

        statusCounts.forEach(  (statusCount) -> {
            PieEntry pieEntry = new PieEntry(statusCount.count());
            pieEntry.setLabel( String.valueOf(statusCount.status()) );
            entries.add(pieEntry);
        } );

        PieDataSet dataSet = new PieDataSet(entries, "Task statistics");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(1f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        List<Integer> colors = new ArrayList<>();

        colors.add(Color.LTGRAY);
        colors.add(Color.CYAN);
        colors.add(Color.MAGENTA);

        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chartTask));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        chartTask.setData(data);

        chartTask.highlightValues(null);

        chartTask.invalidate();
    }
}

