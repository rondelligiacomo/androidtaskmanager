package com.example.task_manager.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.task_manager.R;
import com.example.task_manager.TaskManager;
import com.example.task_manager.persistence.dto.Category;
import com.example.task_manager.view_model.CategoryViewModel;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.editName) EditText editName;

    @Inject ViewModelProvider.Factory viewModelFactory;

    private CategoryViewModel categoryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        ButterKnife.bind(this);

        TaskManager taskManager = (TaskManager) this.getApplication();
        taskManager.applicationComponent().inject(CategoryActivity.this);

        toolbar();

        categoryViewModel = ViewModelProviders.of(CategoryActivity.this, viewModelFactory).get(CategoryViewModel.class);
    }

    private void toolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.buttonOk)
    public void onOk() {
        long identifier = LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli();

        String name = editName.getText().toString();

        Category category = ( new Category.Builder() )
            .identifier(identifier)
            .name(name)
            .build();

        categoryViewModel.create(category);

        hideKeyboard();

        finish();
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

}
