package com.example.task_manager.view;

import android.app.PendingIntent;

import java.util.HashMap;
import java.util.Map;

public class Notification {
    private static Map<Long, PendingIntent> pending = new HashMap<>();

    public static Map<Long, PendingIntent> pending() {
        return (pending);
    }

    public static PendingIntent pending(long identifier) {
        return ( pending.get(identifier) );
    }

    public static void add(long identifier, PendingIntent pendingIntent) {
        pending.put(identifier, pendingIntent);
    }

    public static void cancel(long identifier) {
        PendingIntent pendingIntent = pending(identifier);
        if ( pendingIntent != null ) {
            pendingIntent.cancel();
            pending.remove(identifier);
        }
    }

}
