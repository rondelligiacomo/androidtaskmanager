package com.example.task_manager.repository;

import androidx.lifecycle.LiveData;

import com.example.task_manager.model.StatusCount;
import com.example.task_manager.persistence.dao.Tasks;

import java.util.List;

import javax.inject.Inject;

public class TaskRepository {
    private Tasks tasks;

    @Inject public TaskRepository(Tasks tasks) {
        this.tasks = tasks;
    }

    public void create(com.example.task_manager.persistence.dto.Task task) {
        tasks.create(task);
    }

    public LiveData< List<com.example.task_manager.model.Task> > all() {
        return ( tasks.all() );
    }

    public LiveData<com.example.task_manager.model.Task> byIdentifier(long identifier) {
        return ( tasks.byIdentifier(identifier) );
    }

    public LiveData< List<com.example.task_manager.model.Task> > byDate(int year, int month, int day) {
        return ( tasks.byDate(year, month, day) );
    }

    public LiveData< List<com.example.task_manager.model.Task> > byPriority(int priority) {
        return ( tasks.byPriority(priority) );
    }

    public LiveData< List<com.example.task_manager.model.Task> > byCategory(int category) {
        return ( tasks.byCategory(category) );
    }

    public LiveData< List<StatusCount> > byStatus() {
        return ( tasks.byStatus() );
    }

    public void update(com.example.task_manager.persistence.dto.Task task) {
        tasks.update(task);
    }

    public void delete(com.example.task_manager.persistence.dto.Task task) {
        tasks.delete(task);
    }

}
