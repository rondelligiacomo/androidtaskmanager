package com.example.task_manager.repository;

import androidx.lifecycle.LiveData;

import com.example.task_manager.model.Category;
import com.example.task_manager.persistence.dao.Categories;

import java.util.List;

import javax.inject.Inject;

public class CategoryRepository {
    private Categories categories;

    @Inject public CategoryRepository(Categories categories) {
        this.categories = categories;
    }

    public void create(com.example.task_manager.persistence.dto.Category category) {
        categories.create(category);
    }

    public LiveData<List<Category>> all() {
        return ( categories.all() );
    }

    public LiveData<com.example.task_manager.model.Category> byIdentifier(long identifier) {
        return ( categories.byIdentifier(identifier) );
    }

    public LiveData<com.example.task_manager.model.Category> byName(String name) {
        return ( categories.byName(name) );
    }

    public void update(com.example.task_manager.persistence.dto.Category category) {
        categories.update(category);
    }

    public void delete(com.example.task_manager.persistence.dto.Category category) {
        categories.delete(category);
    }

}
