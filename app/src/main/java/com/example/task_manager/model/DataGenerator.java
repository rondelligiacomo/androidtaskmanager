package com.example.task_manager.model;

import android.widget.Toast;

import com.example.task_manager.persistence.dto.Category;
import com.example.task_manager.persistence.dto.Task;
import com.example.task_manager.view.MainActivity;
import com.example.task_manager.view.NotificationSender;
import com.example.task_manager.view_model.CategoryViewModel;
import com.example.task_manager.view_model.TaskViewModel;

import java.time.LocalDateTime;

public class DataGenerator {
    private static boolean initialized = true;

    private DataGenerator() {

    }

    public static void generate(MainActivity mainActivity, CategoryViewModel categoryViewModel, TaskViewModel taskViewModel) {
        if ( !initialized ) {
            try {
                long identifier1 = 1;
                long identifier2 = 2;

                Category home = (new Category.Builder()).identifier(identifier1).name("Home").build();
                Category work = (new Category.Builder()).identifier(identifier2).name("Work").build();

                categoryViewModel.create(home);
                categoryViewModel.create(work);

                LocalDateTime now = LocalDateTime.now();
                int year = now.getYear();
                int month = now.getMonth().getValue();
                int day = now.getDayOfMonth();

                int hour = now.getHour();
                int minute = now.getMinute();

                long identifier3 = 3;
                long identifier4 = 4;

                Task task1 = (new Task.Builder())
                        .identifier(identifier3)
                        .name("Task-1")
                        .priority(Priority.LOW)
                        .category(identifier1)
                        .date(year, month, day, hour, 59)
                        .status(Status.PENDING)
                        .description("This task #1")
                        .build();

                Task task2 = (new Task.Builder())
                        .identifier(identifier4)
                        .name("Task-2")
                        .priority(Priority.MEDIUM)
                        .category(identifier2)
                        .date(year, month, day, hour, 0)
                        .status(Status.COMPLETED)
                        .description("This task #2")
                        .build();

                taskViewModel.create(task1);
                taskViewModel.create(task2);

                NotificationSender.send(mainActivity, identifier3, year, month, day, hour, 59);

                initialized = true;
            } catch (Exception exception) {
                Toast.makeText(mainActivity, "Failed while initializing database.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
