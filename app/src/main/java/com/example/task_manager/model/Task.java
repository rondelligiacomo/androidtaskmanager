package com.example.task_manager.model;

public class Task {
    private long identifier;
    private String name;
    private Priority priority;
    private long categoryIdentifier;
    private String categoryName;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private Status status;
    private String description;

    public Task(long identifier, String name, Priority priority, long categoryIdentifier, String categoryName, int year, int month, int day, int hour, int minute, Status status, String description) {
        this.identifier = identifier;
        this.name = name;
        this.priority = priority;
        this.categoryIdentifier = categoryIdentifier;
        this.categoryName = categoryName;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.status = status;
        this.description = description;
    }

    public long identifier() {
        return (identifier);
    }

    public String name() {
        return (name);
    }

    public Priority priority() {
        return (priority);
    }

    public long categoryIdentifier() {
        return (categoryIdentifier);
    }

    public String categoryName() {
        return (categoryName);
    }

    public int year() {
        return (year);
    }

    public int month() {
        return (month);
    }

    public int day() {
        return (day);
    }

    public int hour() {
        return (hour);
    }

    public int minute() {
        return (minute);
    }

    public Status status() {
        return (status);
    }

    public void ongoing() {
        this.status = Status.ONGOING;
    }

    public void complete() {
        this.status = Status.COMPLETED;
    }

    public String description() {
        return (description);
    }

}
