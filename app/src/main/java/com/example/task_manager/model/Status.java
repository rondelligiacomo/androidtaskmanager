package com.example.task_manager.model;

public enum Status {
    NO_STATUS, PENDING, ONGOING, COMPLETED
}
