package com.example.task_manager.model;

public enum Priority {
    NO_PRIORITY, LOW, MEDIUM, HIGH, VERY_HIGH;
}
