package com.example.task_manager.model;

public class StatusCount {
    private Status status;
    private int count;

    public StatusCount(Status status, int count) {
        this.status = status;
        this.count = count;
    }

    public Status status() {
        return (status);
    }

    public int count() {
        return (count);
    }

}
