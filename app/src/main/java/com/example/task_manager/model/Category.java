package com.example.task_manager.model;

public class Category {
    private long identifier;
    private String name;

    public Category(long identifier, String name) {
        this.identifier = identifier;
        this.name = name;
    }

    public long identifier() {
        return (identifier);
    }

    public String name() {
        return (name);
    }

    @Override
    public String toString() {
        return ( name() );
    }

}
