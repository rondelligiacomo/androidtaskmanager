package com.example.task_manager.view_model;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.task_manager.repository.CategoryRepository;
import com.example.task_manager.repository.TaskRepository;

public class ViewModelFactory implements ViewModelProvider.Factory {
    private CategoryRepository categoryRepository;
    private TaskRepository taskRepository;

    public ViewModelFactory(CategoryRepository categoryRepository, TaskRepository taskRepository) {
        this.categoryRepository = categoryRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if ( modelClass.isAssignableFrom(CategoryViewModel.class) )
            return ( (T) new CategoryViewModel(categoryRepository) );
        if ( modelClass.isAssignableFrom(TaskViewModel.class) )
            return ( (T) new TaskViewModel(taskRepository) );

        throw ( new IllegalArgumentException("ViewModel not found.") );
    }

}
