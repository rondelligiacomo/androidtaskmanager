package com.example.task_manager.view_model;

import android.os.AsyncTask;

public class AsynchronousTask extends AsyncTask<Void, Void, Void> {
    private AsynchronousOperation asynchronousOperation;

    public AsynchronousTask(AsynchronousOperation asynchronousOperation) {
        this.asynchronousOperation = asynchronousOperation;
    }

    private AsynchronousOperation asynchronousOperation() {
        return (asynchronousOperation);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        asynchronousOperation().perform();
        return (null);
    }

}
