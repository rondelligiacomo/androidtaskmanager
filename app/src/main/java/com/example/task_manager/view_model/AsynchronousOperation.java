package com.example.task_manager.view_model;

@FunctionalInterface
public interface AsynchronousOperation {

    public void perform();

}
