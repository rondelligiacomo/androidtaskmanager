package com.example.task_manager.view_model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.task_manager.model.Category;
import com.example.task_manager.repository.CategoryRepository;
import com.example.task_manager.repository.TaskRepository;

import java.util.List;

import javax.inject.Inject;

public class CategoryViewModel extends ViewModel {
    private CategoryRepository categoryRepository;

    @Inject public CategoryViewModel(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void create(com.example.task_manager.persistence.dto.Category category) {
        categoryRepository.create(category);
        // new AsynchronousTask( () -> categoryRepository.create(category) ).execute();
    }

    public LiveData<List<Category>> all() {
        return ( categoryRepository.all() );
    }

    public LiveData<com.example.task_manager.model.Category> byIdentifier(long identifier) {
        return ( categoryRepository.byIdentifier(identifier) );
    }

    public LiveData<com.example.task_manager.model.Category> byName(String name) {
        return ( categoryRepository.byName(name) );
    }

    public void update(com.example.task_manager.persistence.dto.Category category) {
        categoryRepository.update(category);
        // new AsynchronousTask( () -> categoryRepository.update(category) ).execute();
    }

    public void delete(com.example.task_manager.persistence.dto.Category category) {
        categoryRepository.delete(category);
        // new AsynchronousTask( () -> categoryRepository.delete(category) ).execute();
    }

}
