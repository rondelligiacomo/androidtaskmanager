package com.example.task_manager.view_model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.task_manager.model.StatusCount;
import com.example.task_manager.model.Task;
import com.example.task_manager.repository.TaskRepository;

import java.util.List;

import javax.inject.Inject;

public class TaskViewModel extends ViewModel {
    private TaskRepository taskRepository;

    @Inject public TaskViewModel(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void create(com.example.task_manager.persistence.dto.Task task) {
        taskRepository.create(task);
        // new AsynchronousTask( () -> taskRepository.create(task) ).execute();
    }

    public LiveData< List<Task> > all() {
        return ( taskRepository.all() );
    }

    public LiveData<com.example.task_manager.model.Task> byIdentifier(long identifier) {
        return ( taskRepository.byIdentifier(identifier) );
    }

    public LiveData< List<com.example.task_manager.model.Task> > byDate(int year, int month, int day) {
        return ( taskRepository.byDate(year, month, day) );
    }

    public LiveData< List<com.example.task_manager.model.Task> > byPriority(int priority) {
        return ( taskRepository.byPriority(priority) );
    }

    public LiveData< List<com.example.task_manager.model.Task> > byCategory(int category) {
        return ( taskRepository.byCategory(category) );
    }

    public LiveData< List<StatusCount> > byStatus() {
        return ( taskRepository.byStatus() );
    }

    public void update(com.example.task_manager.persistence.dto.Task task) {
        taskRepository.update(task);
        // new AsynchronousTask( () -> taskRepository.update(task) ).execute();
    }

    public void delete(com.example.task_manager.persistence.dto.Task task) {
        taskRepository.delete(task);
        // new AsynchronousTask( () -> taskRepository.delete(task) ).execute();
    }

}
