package com.example.task_manager.dependency_injection;

import android.app.Application;

import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import com.example.task_manager.persistence.ApplicationDatabase;
import com.example.task_manager.persistence.dao.Categories;
import com.example.task_manager.persistence.dao.Tasks;
import com.example.task_manager.repository.CategoryRepository;
import com.example.task_manager.repository.TaskRepository;
import com.example.task_manager.view_model.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PersistenceModule {
    private ApplicationDatabase applicationDatabase;

    private Categories categories;
    private Tasks tasks;

    private CategoryRepository categoryRepository;
    private TaskRepository taskRepository;

    private ViewModelFactory viewModelFactory;

    public PersistenceModule(Application application) {
        this.applicationDatabase = Room.inMemoryDatabaseBuilder(application, ApplicationDatabase.class).allowMainThreadQueries().build();

        this.categories = applicationDatabase.categories();
        this.tasks = applicationDatabase.tasks();

        categoryRepository = new CategoryRepository(categories);
        taskRepository = new TaskRepository(tasks);

        viewModelFactory = new ViewModelFactory(categoryRepository, taskRepository);
    }

    @Provides @Singleton
    public ApplicationDatabase applicationDatabase() {
        return (applicationDatabase);
    }

    @Provides @Singleton
    public Categories categories() {
        return (categories);
    }

    @Provides @Singleton
    public Tasks tasks() {
        return (tasks);
    }

    @Provides @Singleton
    public CategoryRepository categoryRepository() {
        return (categoryRepository);
    }

    @Provides @Singleton
    public TaskRepository taskRepository() {
        return (taskRepository);
    }

    @Provides @Singleton
    public ViewModelProvider.Factory viewModelFactory() {
        return (viewModelFactory);
    }

}
