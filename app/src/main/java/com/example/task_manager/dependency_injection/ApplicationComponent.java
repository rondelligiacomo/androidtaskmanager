package com.example.task_manager.dependency_injection;

import android.app.Application;

import com.example.task_manager.persistence.ApplicationDatabase;
import com.example.task_manager.persistence.dao.Categories;
import com.example.task_manager.persistence.dao.Tasks;
import com.example.task_manager.repository.CategoryRepository;
import com.example.task_manager.repository.TaskRepository;
import com.example.task_manager.view.CategoryActivity;
import com.example.task_manager.view.MainActivity;
import com.example.task_manager.view.SearchActivity;
import com.example.task_manager.view.StatisticsActivity;
import com.example.task_manager.view.TaskActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component( modules = { PersistenceModule.class, ApplicationModule.class } ) @Singleton
public interface ApplicationComponent {

    // Application
    public Application application();
    // ---

    // Activities
    public void inject(MainActivity mainActivity);
    public void inject(CategoryActivity categoryActivity);
    public void inject(TaskActivity taskActivity);
    public void inject(SearchActivity searchActivity);
    public void inject(StatisticsActivity statisticsActivity);
    // ---

    // Database
    public ApplicationDatabase applicationDatabase();
    public Categories categories();
    public Tasks tasks();
    // ---

    // Repository
    public CategoryRepository categoryRepository();
    public TaskRepository taskRepository();
    // ---

}
