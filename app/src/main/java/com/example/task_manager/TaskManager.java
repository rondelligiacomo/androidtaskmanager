package com.example.task_manager;

import android.app.Application;

import com.example.task_manager.dependency_injection.ApplicationComponent;
import com.example.task_manager.dependency_injection.ApplicationModule;
import com.example.task_manager.dependency_injection.DaggerApplicationComponent;
import com.example.task_manager.dependency_injection.PersistenceModule;

public class TaskManager extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule( new ApplicationModule(this) )
            .persistenceModule( new PersistenceModule(this) )
            .build();
    }

    public ApplicationComponent applicationComponent() {
        return (applicationComponent);
    }

}
